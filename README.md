# Mojtaba's Digital Garden

Welcome to my digital garden! This is a space where I explore ideas, share notes, and grow my knowledge in public. Built with [Quartz](https://quartz.jzhao.xyz/), this garden is a living, breathing collection of my thoughts and learnings.

## About Me

I'm Mojtaba, and I'm excited about the potential of digital gardens for learning and sharing knowledge. I'm still figuring out how to effectively use and grow this space, but I'm enjoying the journey so far.

Some highlights:

- Seamless integration with [Obsidian](https://obsidian.md/) for managing my notes
- Easy deployment using [GitLab Pages](https://gitlab.com/myousefi/notes)
- Automated CI/CD pipeline that backs up my Obsidian vault daily and deploys changes
- Ability to interact with my notes using the [Cody](https://sourcegraph.com/cody) extension in VS Code

## What You'll Find Here

In this garden, you'll find a variety of content, including:

- Notes on the [[books]] I read
- Daily thoughts and musings
- Longer-form posts on various topics
- And more!

The structure of the garden is as follows:

```
├── content
│ ├── \_index.md
│ ├── banner.svg
│ ├── books
│ ├── books.md
│ ├── daily
│ ├── posts
│ ├── tags
│ ├── templates
│ └── thoughts
```

- `content`: The main directory for all my notes and content
  - `_index.md`: The home page of the garden
  - `banner.svg`: The banner image displayed on the home page
  - `books`: Notes on books I've read
  - `daily`: Daily notes and thoughts
  - `posts`: Longer-form blog-style posts
  - `tags`: Pages for each tag used in the garden
  - `templates`: Template files for different note types
  - `thoughts`: Shorter, more informal notes and ideas

## CI/CD Pipeline

This garden is powered by a GitLab CI/CD pipeline defined in `.gitlab-ci.yml`. The pipeline handles:

- Merging changes from the `develop` branch to `main` on a schedule or manual trigger
- Building the garden using Quartz
- Deploying the built site to GitLab Pages

This automation makes it easy to keep the garden up to date and ensures that my Obsidian vault is always in sync with the published site.

## Acknowledgements

This digital garden is enabled and inspired by [Jacky Zhao](https://github.com/jackyzha0) and his amazing [quartz](https://github.com/jackyzha0/quartz) library. Thank you, Jacky, for creating such a wonderful tool for growing digital gardens!

## Explore and Enjoy

Feel free to explore the garden, and connect with me. I always reply to my emails at [m@ysfi.me](mailto:m@ysfi.me?subject=Digital%20Garden). I'm always eager to learn and discuss new ideas. Happy gardening!

```poetry
- Mojtaba :)
```
