---
title: "Notes"
tags:
  - evergreen
---

<div style="width: 100%;">
  <img src="./banner.svg" width="auto" height="200" style="margin-left: 0; margin-right: auto;">
</div>

```poetry
Hi!
```

I'm Mojtaba. This is my little digital garden on the Internet!

I am still in the exploration phase, and I have yet to figure out how to grow, and effectively use a digital garden. So far, I am excited about the seamless integration with [Obsidian](https://obsidian.md/), the ease of deploying my notes using [GitLab Pages](https://gitlab.com/myousefi/notes), and the CI/CD pipeline that automatically backs up my Obsidian vault using git into the develop branch, and deploys changes daily by a scheduled merge into the main branch.

What's more? I can talk to my notes in VS Code, using the amazing [Cody](https://sourcegraph.com/cody) extension which indexes my notes in the embedding space provided by OpenAI.

I try to keep my notes on the [[books]] I read here.

This digital garden is enabled and inspired by [Jacky Zhao](https://github.com/jackyzha0) and his amazing [quartz](https://github.com/jackyzha0/quartz) library.

```poetry
- Mojtaba :)
```
