```mermaid
graph TD
  orchestration[Orchestration]
  swarm[("Swarm\nManages clusters (swarms) of Docker nodes")]
  cli[("CLI")]
  api[("API")]
  imageMgt[("Image mgt")]
  networking[("Networking")]
  volumes[("Volumes")]
  ellipsis[("...")]
  containerd[("containerd\nhigh-level runtime – lifecycle management")]
  runc[("runc\nLow-level runtime – kernel-level work")]

  orchestration --> swarm
  orchestration --> cli
  orchestration --> networking
  cli --> api
  api --> imageMgt
  networking --> volumes
  volumes --> ellipsis
  networking --> containerd
  cli --> containerd
  imageMgt --> runc
  ellipsis --> runc

```
