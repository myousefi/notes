---
title: "GitHub Actions"
date: 2022-11-29
tags:
  - seed
---

GitHub allows to use self-hosted runners for CI/CD.

It is recommended not to use a self-hosted runner on public repositories as forks can run unauthorized code on the machine.

A self-hosted runner needs to be labeled according to the workflow files so it can pick up the jobs. For example, if a workflow is:

```
jobs:
	deploy:
		runs-on:
			- self-hosted
			- production
```

The runner needs to be labeled with `self-hosted` and `production` labels.

[[thoughts/Environment Variable]]
