---
title: Environment Variable
date: 2024-04-26
tags:
  - seed
---
One cool idea to handle .env files:

https://github.com/tiangolo/full-stack-fastapi-template/issues/724#issuecomment-1999566151

### Step 1: Encode the `.env` File

You can encode your `.env` file to a base64 string using a command-line tool like `base64`:

```bash
base64 .env > .env.base64
```

### Step 2: Store the Encoded String in GitHub Secrets

- Go to your GitHub repository.
- Navigate to **Settings** > **Secrets** > **Actions**.
- Click on **New repository secret**.
- Name your secret (e.g., `ENCODED_ENV`) and paste the content of `.env.base64`.

### Step 3: Modify GitHub Actions to Decode and Use the `.env` File

In your GitHub Actions workflow, you can add steps to decode the base64 string and create an `.env` file during the build or deployment process:

```yaml
jobs:
  build:
    runs-on: ubuntu-latest
    steps:
      - name: Checkout
        uses: actions/checkout@v2

      - name: Decode .env file
        run: echo "${{ secrets.ENCODED_ENV }}" | base64 --decode > .env

      - name: Use .env file
        run: |
          set -a
          source .env
          set +a
          # Now you can use the environment variables from the .env file
```
I am going to give a try, it can further be streamlined through a git pre-commit hook to always encode and keep the latest .env variables into repo