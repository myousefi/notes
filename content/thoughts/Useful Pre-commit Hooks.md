---
title: Useful Pre-commit Hooks
date: 2024-04-28
tags:
  - seed
---
I can't remember how many times I had to rebase, reset, or clear a git repository history due to someone unintentionally committing large files which couldn't be pushed afterward. This pre-commit hook does exactly take care of this:

```yaml
-   repo: https://github.com/pre-commit/pre-commit-hooks
    rev: v4.6.0
    hooks:
    -   id: check-added-large-files
        args: ['--maxkb=1000']  # Adjust max file size as needed
```

Here is a repo to some other useful pre-commit hooks:

**[pre-commit-hooks](https://github.com/pre-commit/pre-commit-hooks)**