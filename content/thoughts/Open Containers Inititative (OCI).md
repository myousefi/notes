
The OCI is a governance council responsible for standardizing the low-level fundamen-
tal components of container infrastructure. In particular it focusses on image format and
container runtime (don’t worry if you’re not comfortable with these terms yet, we’ll cover
them in the book).