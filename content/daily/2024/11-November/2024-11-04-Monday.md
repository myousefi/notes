---
title: 2024-11-04 09:36
date: 2024-11-04
tags:
  - daily
---

# Monday, November 04, 2024

<< [[Timestamps/2024/11-November/2024-11-03-Sunday|Yesterday]] | [[Timestamps/2024/11-November/2024-11-05-Tuesday|Tomorrow]] >>

---

People in New Media Art:

[watershed](https://lakeheckaman.substack.com/)

[Lake Heckaman - June 2024 Update](https://lakeheckaman.substack.com/p/lake-heckaman-june-2024-update)

New York City exhibition next week, and more

[![](https://substackcdn.com/image/fetch/w_80,c_limit,f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2Fb8f755c7-a656-44b2-a60a-01e8f0026ee7_144x144.png)](https://substack.com/profile/3108447-lake)

[lake](https://substack.com/@lakeheckaman)

Jun 13, 2024

## About

Mehdi Farahani is a multidisciplinary and new media artist, musician, designer, and writer whose work resides at the dynamic crossroads of art and technology. A seasoned design professional, he specializes in cutting-edge, interactive approaches utilizing emerging technologies. His expertise encompasses music visualization through artificial intelligence, offering a unique perspective in this evolving field.  
  
In addition to his innovative expertise encompassing computational design, UX/UI design, animation, motion graphics, sound design, and 3D modeling, Mehdi demonstrates mastery of traditional artistic mediums. His mastery in traditional design, typography, illustration, and print reflects a deep-rooted proficiency that complements his innovative, cutting-edge skill set.  
  
Throughout his career, Mehdi has effectively managed a spectrum of projects, contributing significantly to the growth and success of various international organizations. His impact spans diverse ventures, from start-ups to established growth-stage companies, showcasing his ability to navigate and elevate creative endeavors across the board.

[![Writer's picture](https://static.wixstatic.com/media/a699af_9139d950e7294a4c8f053f1f22598fa9~mv2.jpg_srz_320_320_75_22_0.5_1.20_0.00_jpg_srz)Steve Sangapore](https://www.sangapore.com/profile/a699af4f-0510-4426-b0a3-5a48762db120/profile)

Apr 5, 20237 min read

# BUT, IT’S BEAUTIFUL! Why Artificial Intelligence Can’t Make Art

## Description

“Choice” is a multidimensional interactive artwork from the collection “Our Behaviour Shapes Our Reality,” merging Darwinian evolutionary theory with data-driven art. This piece employs an ESP32 microcontroller, gyroscope, and accelerometer sensors to capture viewer movements, which are then used to autonomously generate the digital art displayed.