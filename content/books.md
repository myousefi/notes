---
title: Booklist
aliases:
  - Reading list
---

## To Read

> What is an [antilibrary](https://nesslabs.com/antilibrary)? To put it simply, an antilibrary is a private collection of unread books. Instead of a celebration of everything you know, an antilibrary is an ode to everything you want to explore.

### Fiction

### Non-fiction

### Poetry

## Current

**[[books/Docker Deep Dive Zero to Docker in a Single Book]]** by [Docker Captain Nigel Poulton](https://twitter.com/nigelpoulton)
[[books/Designing Data-Intensive Applications The Big Ideas Behind Reliable, Scalable, and Maintainable Systems]] by [Dr. Martin Kleppmann](https://bsky.app/profile/martin.kleppmann.com)

## Past