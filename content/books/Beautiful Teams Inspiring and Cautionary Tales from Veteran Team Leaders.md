---
title: "Beautiful Teams: Inspiring and Cautionary Tales from Veteran Team Leaders"
date: 2024-04-29
tags:
  - seed
---
I have developed a habit of re-reading books I had read some time but forgotten the details or just miss them. This book is in this list, and this time I try to take notes.
