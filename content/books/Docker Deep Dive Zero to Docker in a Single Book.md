
[[VMware]] created the first virtual machine technology. Before that any server had to run one single business application.

Some of the major technologies that enabled the massive growth of containers in recent  
years include; [[kernel namespaces]], [[control groups]], [[capabilities]], and of course [[Docker]].

The company started out as a platform as a service (PaaS) provider called dotCloud.  
Behind the scenes, the dotCloud platform was built on Linux containers. To help create  
and manage these containers, they built an in-house tool that they eventually nick-  
named “Docker”. And that’s how the Docker technology was born!  

It’s also interesting to know that the word “[[Docker]]” comes from a British expression  
meaning dock worker — somebody who loads and unloads cargo from ships.

There are four things that come to mind when talking about [[Docker]]:
	1. Docker, Inc.
	2. The runtime
	3. The daemon
	4. The orchestrator

The docker runtime is tiered: High (containerd) / Low (runc)  levels

[[runcs]] implements the [[Open Containers Inititative (OCI)]] 


[[Play with Docker (PWD)]]

In a default Linux installation, the client talks to the daemon via a local IPC/Unix
socket at /var/run/docker.sock. On Windows this happens via a named pipe at
npipe:////./pipe/docker_engine. Once installed, you can use the docker version
command to test that the client and daemon (server) are running and talking to each
other.

A Docker image can be conceived as an object that contains an OS filesystem, an application, and all application dependencies. For an Ops person it is like a virtual machine template. For a Dev person it is a *class*.

