---
title: "Dev Setup on MacOs"
subtitle: "less distraction with the mouse more time on keyboard and more productivity"
date: 2024-05-03
tags:
  - seed
---

Nearly a year has passed since I transitioned to the Apple ecosystem, and I have no plans to revert. After abandoning Windows in 2017, I embraced Manjaro Linux with i3 on my Surface Book, despite its limited Linux support. My switch to Apple was driven by the impressive line of ARM-based MacBooks and Apple's focus on end-user experience, which has resulted in superior hardware and software integration. While I still appreciate Linux for servers and development, MacOS has become my personal OS of choice.

Previously, I managed my development setup through GitHub-hosted dotfiles. However, I'm still figuring out the best way to track my MacOS configuration. This note serves to document my current tools and setup. I frequently experiment with new tools each month, retaining those that fit my workflow and discarding others when storage becomes an issue.

[Raycast](https://www.raycast.com/) has become indispensable, enhancing MacOS's Spotlight with deeper integrations like [Todoist](https://todoist.com), and a robust clipboard manager through [Raycast Clipboard](https://www.raycast.com/extensions/clipboard-history). Other notable tools include [Gemini](https://www.raycast.com/EvanZhouDev/raycast-gemini), [Phind](https://www.raycast.com/Chuck/phind-search), [Perplexity](https://www.raycast.com/third774/perplexity), and [VSCode](https://www.raycast.com/thomas/visual-studio-code).

I missed the tile window management of i3, but [Amethyst](https://github.com/ianyh/Amethyst) for MacOS has filled that gap effectively. Although there was a learning curve, I no longer miss i3. For Vimium-like navigation, I've adopted [Shortcat](https://shortcat.app/).

One limitation of the MacBook Air M2 is its native support for only one external display. I've resorted to using [DisplayLink](https://www.synaptics.com/products/displaylink-graphics/downloads/macos) technology with an expensive adapter to overcome this, though it introduces some lag. The ARM-based processor is excellent, and I appreciate the quiet operation without fan noise. I've also set up SSH configurations to run VSCode on Northeastern's slurm-based HPC clusters for tasks requiring additional compute or memory.

Choosing a MacBook with only 512GB of storage was intended to prevent data hoarding, but in hindsight, it may not have been the best decision due to frequent storage issues.
