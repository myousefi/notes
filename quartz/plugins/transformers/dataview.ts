import { Component, Notice } from "obsidian"
import { DataviewApi, getAPI } from "obsidian-dataview"
// import Logger from "js-logger"
import { QuartzTransformerPlugin } from "../types"

function escapeRegExp(string: string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&") // $& means the whole matched string
}

export const DataviewCompiler: QuartzTransformerPlugin = () => {
  return {
    name: "DataviewCompiler",
    textTransform(_ctx, src) {
      if (src instanceof Buffer) {
        src = src.toString()
      }

      let replacedText = src
      const dataViewRegex = /```dataview\s(.+?)```/gms
      const dvApi = getAPI()

      if (!dvApi) return replacedText

      const matches = src.matchAll(dataViewRegex)
      const dataviewJsPrefix = dvApi.settings.dataviewJsKeyword
      const dataViewJsRegex = new RegExp(
        "```" + escapeRegExp(dataviewJsPrefix) + "\\s(.+?)```",
        "gsm",
      )
      const dataviewJsMatches = src.matchAll(dataViewJsRegex)
      const inlineQueryPrefix = dvApi.settings.inlineQueryPrefix
      const inlineDataViewRegex = new RegExp(
        "`" + escapeRegExp(inlineQueryPrefix) + "(.+?)`",
        "gsm",
      )
      const inlineMatches = src.matchAll(inlineDataViewRegex)
      const inlineJsQueryPrefix = dvApi.settings.inlineJsQueryPrefix
      const inlineJsDataViewRegex = new RegExp(
        "`" + escapeRegExp(inlineJsQueryPrefix) + "(.+?)`",
        "gsm",
      )
      const inlineJsMatches = src.matchAll(inlineJsDataViewRegex)

      if (!matches && !inlineMatches && !dataviewJsMatches && !inlineJsMatches) {
        return src
      }

      //Code block queries
      for (const queryBlock of matches) {
        try {
          const block = queryBlock[0]
          const query = queryBlock[1]
          const { isInsideCallout, finalQuery } = sanitizeQuery(query)
          let markdown = dvApi.tryQueryMarkdown(finalQuery, "")
          if (isInsideCallout) {
            markdown = surroundWithCalloutBlock(markdown)
          }
          replacedText = replacedText.replace(block, `${markdown}`)
        } catch (e) {
          console.log(e)
          new Notice(
            "Unable to render dataview query. Please update the dataview plugin to the latest version.",
          )
          return queryBlock[0]
        }
      }

      for (const queryBlock of dataviewJsMatches) {
        try {
          const block = queryBlock[0]
          const query = queryBlock[1]
          const div = createEl("div")
          const component = new Component()
          component.load()
          dvApi.executeJs(query, div, component, "")
          let counter = 0
          while (!div.querySelector("[data-tag-name]") && counter < 100) {
            delay(5)
            counter++
          }
          replacedText = replacedText.replace(block, div.innerHTML ?? "")
        } catch (e) {
          console.log(e)
          new Notice(
            "Unable to render dataviewjs query. Please update the dataview plugin to the latest version.",
          )
          return queryBlock[0]
        }
      }

      //Inline queries
      for (const inlineQuery of inlineMatches) {
        try {
          const code = inlineQuery[0]
          const query = inlineQuery[1]
          const dataviewResult = dvApi.tryEvaluate(query.trim(), {
            this: {},
          })
          if (dataviewResult) {
            replacedText = replacedText.replace(code, dataviewResult.toString() ?? "")
          }
        } catch (e) {
          console.log(e)
          new Notice(
            "Unable to render inline dataview query. Please update the dataview plugin to the latest version.",
          )
          return inlineQuery[0]
        }
      }

      for (const inlineJsQuery of inlineJsMatches) {
        try {
          const code = inlineJsQuery[0]
          const query = inlineJsQuery[1]
          let result: string | undefined | null = ""
          result = tryDVEvaluate(query, dvApi)
          if (!result) {
            result = tryEval(query)
          }
          if (!result) {
            result = tryExecuteJs(query, dvApi)
          }
          replacedText = replacedText.replace(code, result ?? "Unable to render query")
        } catch (e) {
          //   Logger.error(e)
          new Notice(
            "Unable to render inline dataviewjs query. Please update the dataview plugin to the latest version.",
          )
          return inlineJsQuery[0]
        }
      }

      return replacedText
    },
  }
}

function sanitizeQuery(query: string): {
  isInsideCallout: boolean
  finalQuery: string
} {
  let isInsideCallout = false
  const parts = query.split("\n")
  const sanitized = []

  for (const part of parts) {
    if (part.startsWith(">")) {
      isInsideCallout = true
      sanitized.push(part.substring(1).trim())
    } else {
      sanitized.push(part)
    }
  }
  let finalQuery = query

  if (isInsideCallout) {
    finalQuery = sanitized.join("\n")
  }

  return { isInsideCallout, finalQuery }
}

function surroundWithCalloutBlock(input: string): string {
  const tmp = input.split("\n")
  return " " + tmp.join("\n> ")
}

function tryDVEvaluate(query: string, dvApi: DataviewApi): string | undefined | null {
  let result = ""
  try {
    const dataviewResult = dvApi.tryEvaluate(query.trim(), {
      this: {},
    })
    result = dataviewResult?.toString() ?? ""
  } catch (e) {
    // Logger.warn("dvapi.tryEvaluate did not yield any result", e)
  }
  return result
}

function tryEval(query: string) {
  let result = ""
  try {
    result = (0, eval)("const dv = DataviewAPI;" + query)
  } catch (e) {
    // Logger.warn("eval did not yield any result", e)
  }
  return result
}

function tryExecuteJs(query: string, dvApi: DataviewApi) {
  const div = createEl("div")
  const component = new Component()
  component.load()
  dvApi.executeJs(query, div, component, "")
  let counter = 0
  while (!div.querySelector("[data-tag-name]") && counter < 50) {
    delay(5)
    counter++
  }
  return div.innerHTML
}

function delay(milliseconds: number) {
  return new Promise((resolve, _) => {
    setTimeout(resolve, milliseconds)
  })
}
